import * as _ from "lodash";
import Card from "./Card";

const RANKS = [
  "Ace",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "Jack",
  "King",
];
const SUITS = ["Hearts", "Clubs", "Spades", "Diamonds"];

export default class Deck {
  private cards: Card[];
  private currentCardIndex: number;

  constructor() {
    this.currentCardIndex = 0;
    this.cards = _.reduce(
      RANKS,
      (acc: Card[], rank: string) => {
        const cards = _.map(SUITS, (suit: string) => {
          return new Card(rank, suit);
        });
        return _.concat(acc, cards);
      },
      [],
    );
    this.shuffleDeck();
  }

  public shuffleDeck = () => {
    this.cards = _.shuffle(this.cards);
  }

  public draw = (): Card => {
    this.currentCardIndex++;
    return this.cards[this.currentCardIndex];
  }
}
