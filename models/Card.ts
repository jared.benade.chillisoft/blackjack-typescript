export default class Card {
  public rank: string;
  public suit: string;
  public value: number;

  constructor(rank: string, suit: string) {
    this.rank = rank;
    this.suit = suit;

    switch (rank) {
      case "Ace":
        this.value = 11;
        break;
      case "King":
      case "Queen":
      case "Jack":
        this.value = 10;
        break;
      default:
        this.value = parseInt(rank);
        break;
    }
  }

  public toString = () => {
    return `${this.rank} of ${this.suit}`;
  }
}
