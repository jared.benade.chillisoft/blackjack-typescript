export const newRoundQuestion = [
  {
    type: "list",
    name: "newRound",
    message: "Do you want to play another round?",
    choices: ["Yes", "No"],
  },
];

export const turnQuestion = [
  {
    type: "list",
    name: "turn",
    message: "What would you like to do?",
    choices: ["Hit", "Stay"],
  },
];
