import Card from "./Card";

export default class Player {
  public name: string;
  public hand: Card[];

  constructor(name: string) {
    this.name = name;
    this.hand = [];
  }
}
