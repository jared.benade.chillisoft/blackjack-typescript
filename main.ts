import BlackjackGame from "./BlackjackGame";

function init() {
  const game = new BlackjackGame();
  game.start();
}

init();
