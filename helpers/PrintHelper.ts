import chalk from "chalk";
import Player from "../models/Player";
import CardHelper from "./CardHelper";

class PrintHelper {
  public printStartingCards = (dealer: Player, player: Player) => {
    this.printDealerStartingCard(dealer);
    this.printPlayerStartingCards(player);
  }

  public printDealerStartingCard = (dealer: Player) => {
    console.log(
      `The dealer has a ${chalk.yellow(dealer.hand[0].toString())}\n`,
    );
  }

  public printDealerSecondCard = (dealer: Player) => {
    console.log(
      `The dealer's second card is a ${chalk.yellow(dealer.hand[1].toString())}`,
    );
    console.log(
      `The dealer's current hand value is ${chalk.magenta(
        CardHelper.getHandValue(dealer).toString(),
      )}\n`,
    );
  }

  public printPlayerStartingCards = (player: Player) => {
    console.log(
      `You have a ${chalk.yellow(
        player.hand[0].toString(),
      )} and a ${chalk.yellow(player.hand[1].toString())}`,
    );
    console.log(
      `Your current hand value is ${chalk.magenta(
        CardHelper.getHandValue(player).toString(),
      )}\n`,
    );
  }

  public printLastDraw = (player: Player) => {
    const lastCardIndex = player.hand.length - 1;
    console.log(
      `\nYou drew a ${chalk.yellow(player.hand[lastCardIndex].toString())}`,
    );
    console.log(
      `Your current hand value is ${chalk.magenta(
        CardHelper.getHandValue(player).toString(),
      )}\n`,
    );
  }

  public printDealersLastDraw = (dealer: Player) => {
    const lastCardIndex = dealer.hand.length - 1;
    console.log(
      `The dealer drew a ${chalk.yellow(dealer.hand[lastCardIndex].toString())}`,
    );
    console.log(
      `The dealer's current hand value is ${chalk.magenta(
        CardHelper.getHandValue(dealer).toString(),
      )}\n`,
    );
  }

  public printWelcomeMessage = () => {
    console.log("======================");
    console.log(" Welcome to Blackjack");
    console.log("======================\n");
  }
}

const printHelper = new PrintHelper();
export default printHelper;
