import Card from "../models/Card";
import Deck from "../models/Deck";
import Player from "../models/Player";
import CardHelper from "./CardHelper";

describe("CardHelper", () => {
  describe("draw", () => {
    it("should call draw on deck", () => {
      // Arrange
      const deck = new Deck();
      deck.draw = jest.fn();
      const player = new Player("");
      // Act
      CardHelper.draw(deck, player);
      // Assert
      expect(deck.draw).toHaveBeenCalled();
    });
    it("should push drawn card to player hand", () => {
      // Arrange
      const deck = new Deck();
      const card = new Card("Ace", "Spades");
      deck.draw = jest.fn(() => card);
      const player = new Player("");
      // Act
      CardHelper.draw(deck, player);
      // Assert
      expect(player.hand).toContain(card);
    });
  });
});
