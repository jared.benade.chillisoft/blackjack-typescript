import * as _ from "lodash";
import Card from "../models/Card";
import Deck from "../models/Deck";
import Player from "../models/Player";

class CardHelper {
  public deal = (deck: Deck, players: Player[]) => {
    _.each(players, (player: Player) => {
      for (let i = 0; i < 2; i++) {
        this.draw(deck, player);
      }
    });
  }

  public draw = (deck: Deck, player: Player) => {
    const nextCard = deck.draw();
    player.hand.push(nextCard);
  }

  public getHandValue = (player: Player) => {
    let handValue = 0;
    _.each(player.hand, (card: Card) => {
      handValue += card.value;

      if (handValue > 21 && card.rank === "Ace") {
        handValue -= 10;
      }
    });

    return handValue;
  }
}

const cardHelper = new CardHelper();
export default cardHelper;
