import chalk from "chalk";
import * as inquirer from "inquirer";
import * as _ from "lodash";
import CardHelper from "./helpers/CardHelper";
import PrintHelper from "./helpers/PrintHelper";
import Deck from "./models/Deck";
import Player from "./models/Player";
import { newRoundQuestion, turnQuestion } from "./models/Questions";

export default class BlackjackGame {
  public deck: Deck;
  public players: Player[];
  public currentPlayerIndex: number;

  constructor() {
    this.deck = new Deck();
    this.players = [new Player("Dealer"), new Player("The Player")];
    this.currentPlayerIndex = 1;
  }

  public start() {
    console.clear();
    this.setDefaultState();
    const dealer = this.players[0];
    const player = this.players[1];

    PrintHelper.printWelcomeMessage();
    CardHelper.deal(this.deck, this.players);
    PrintHelper.printStartingCards(dealer, player);
    this.evaluateGameState();
  }

  public newRoundUserPrompt = () => {
    inquirer.prompt(newRoundQuestion).then((answers: any) => {
      switch (answers.newRound) {
        case "Yes":
          this.start();
          break;
        case "No":
          console.log("\nThanks for playing. Goodbye.\n");
          break;
      }
    });
  }

  public turnUserPrompt = () => {
    inquirer.prompt(turnQuestion).then((answers: any) => {
      switch (answers.turn) {
        case "Hit":
          this.hit();
          break;
        case "Stay":
          this.stay();
          break;
      }
    });
  }

  public hit = () => {
    const currentPlayer = this.getCurrentPlayer();
    CardHelper.draw(this.deck, currentPlayer);
    PrintHelper.printLastDraw(currentPlayer);
    this.evaluateGameState();
  }

  public stay = () => {
    console.log("\nYou chose to stay.\n");
    this.currentPlayerIndex = 0;
    this.evaluateGameState();
  }

  public evaluateGameState = () => {
    let winner = new Player("");
    let draw = true;

    const currentPlayer = this.getCurrentPlayer();
    const currentPlayerIsDealer = this.currentPlayerIndex === 0;
    if (currentPlayerIsDealer) {
      PrintHelper.printDealerSecondCard(currentPlayer);

      const playerHasStartingHandBlackjack =
        this.players[1].hand.length === 2 &&
        CardHelper.getHandValue(this.players[1]) === 21;

      if (!playerHasStartingHandBlackjack) {
        while (CardHelper.getHandValue(currentPlayer) < 17) {
          CardHelper.draw(this.deck, currentPlayer);
          PrintHelper.printDealersLastDraw(currentPlayer);
          if (CardHelper.getHandValue(currentPlayer) > 21) {
            console.log(chalk.red("The Dealer has bust!\n"));
            break;
          }
        }
      }

      _.each(this.players, (player) => {
        if (CardHelper.getHandValue(player) <= 21) {
          if (
            CardHelper.getHandValue(player) > CardHelper.getHandValue(winner)
          ) {
            winner = player;
            draw = false;
          } else if (
            CardHelper.getHandValue(player) === CardHelper.getHandValue(winner)
          ) {
            draw = true;
          }
        }
      });

      if (draw) {
        console.log(`This round was a tie. Dealer wins.\n`);
      } else {
        console.log(
          `${winner.name} won the round with ${CardHelper.getHandValue(
            winner,
          )}.\n`,
        );
      }
      this.newRoundUserPrompt();
    } else {
      if (CardHelper.getHandValue(currentPlayer) > 21) {
        console.log(chalk.red("Bust! You lose.\n"));
        this.newRoundUserPrompt();
      } else {
        if (currentPlayer.hand.length === 5) {
          console.log("You have a five card trick!! You win.\n");
          this.newRoundUserPrompt();
        } else {
          if (CardHelper.getHandValue(currentPlayer) === 21) {
            console.log("Blackjack!!\n");
            this.currentPlayerIndex = 0;
            this.evaluateGameState();
          } else {
            this.turnUserPrompt();
          }
        }
      }
    }
  }

  private setDefaultState = () => {
    this.deck = new Deck();
    this.players = [new Player("Dealer"), new Player("The Player")];
    this.currentPlayerIndex = 1;
  }

  private getCurrentPlayer = () => {
    return this.players[this.currentPlayerIndex];
  }
}
